[a]-- All artists that has letter D in its name
SELECT * FROM artists WHERE name LIKE "%d%";

[b] -- Songs that has length of less then 230 
SELECT * FROM songs WHERE length < 230;

[c] -- Joined "albums" and "songs" table
SELECT albums.album_title, songs.song_name, songs.length
FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

[d] -- Joined "artists" and "albums" tables
SELECT albums.id, albums.album_title
FROM albums
JOIN artists ON artists.id = albums.artist_id
WHERE albums.album_title LIKE "%a%";

[e] -- Sort the albums in Z-A order (Show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

[f] -- Joined "albums" and "songs" tables (sorted from Z-A)
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY albums.id DESC;